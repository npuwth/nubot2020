# RTDB的用法

## 原理





### **World_model相关**

​	参数就是更改数据的来源

1）updateStrategyinfo(const nubot_common::StrategyInfo &strategyinfo)



从nubot_control信息，写到teammatesinfo_中通过RTDB进行信息传输 

**更新**：robot内容，策略的信息，包括发起的传球命令等等



```c++
class Teammatesinfo

{

public:

  Robot     robot_info_;

  BallObject   ball_info_;

  ObstacleObject obs_info_[MAX_OBSNUMBER_CONST];

  PassCommands  pass_cmds_;

};

```

2） updateKinectBall(const nubot_common::BallInfo3d & _ball_info)



3）updateFrontVision(const nubot_common::FrontBallInfo & _front_info)



4） updateOminivision(const nubot_common::OminiVisionInfo & omni_info)

**更新**：robot内容，障碍物信息，足球信息



5)  update 以及其之后的函数（1.31  重中之重）

**world_model中相关引用rtdb函数：**



DB_get()

DB_init   RTDB通信模块的初始化，开辟内存空间

DB_free   RTDB通信模块的释放开辟内存空间

​		释放三块空间：shared，local，def



DB_put_in()   自身感知的信息发布给队友





**信息的有效性**指的是：

​	记录获取信息的时间间隔，当前时间戳，判断下一次某一个时间时是否已经获取了新的信息。













### 核心数据结构与函数



三个内存区：

​	shared  

​    local

​    def



1）linux 内核 为每一个共享内存段维护的一个**特殊的重要的**数据结构

```c++
struct shmid_ds{
      struct ipc_perm shm_perm;/* 操作权限*/
       int shm_segsz;                    /*段的大小（以字节为单位）*/
      time_t shm_atime;          /*最后一个进程附加到该段的时间*/
       time_t shm_dtime;          /*最后一个进程离开该段的时间*/
      time_t shm_ctime;          /*最后一个进程修改该段的时间*/
      unsigned short shm_cpid;   /*创建该段进程的pid*/
       unsigned short shm_lpid;   /*在该段上操作的最后1个进程的pid*/
       short shm_nattch;          /*当前附加到该段的进程的个数*/
/*下面是私有的*/
        unsigned short shm_npages;  /*段的大小（以页为单位）*/
      unsigned long *shm_pages;   /*指向frames->SHMMAX的指针数组*/
      struct vm_area_struct *attaches; /*对共享段的描述*/
};
```



2）RTDBdef 全局的rtdb信息

```c++
typedef struct
{
  int self_agent;						// numero do agente onde esta a correr  己方机器人数目
  int n_agents;						// numero total de agentes registados  agents的数目
  int n_shared_recs[MAX_AGENTS];		// numero de 'variaveis' shared  共享变量数
  int n_local_recs;					// numero de 'variaveis' local  本地变量数
  int shared_mem_size[MAX_AGENTS];	// tamanho das areas shared  共享区域大小
  int local_mem_size;					// tamanho da area local  本地区域大小
  int rec_lut[MAX_AGENTS][MAX_RECS];	// lookuptable    查表
} RTDBdef;
```



3）传输的每一个数据的结构

```c++
typedef struct
{
  int id;							// id da 'variavel'  标识符
  int size;						// sizeof da 'variavel'  数据大小
  int period;						// refresh period for broadcast  数据的更新周期
  int offset;						// offset para o campo de dados da 'variavel'  数据开始位置（字段）
  int read_bank;					// variavel mais actual   读写标志
  struct timeval timestamp[2];	// relogio da maquina local  时间戳
} TRec;
```







3）Linux IPC ： 进程之间的通信



4）共享内存函数（shmget、shmat、shmdt、shmctl）

int shmget(key_t key, size_t size, int shmflg)   

​	得到一个共享内存标识符或创建一个共享内存对象并返回共享内存标识符



void *shmat(int shmid, const void *shmaddr, int shmflg)

​	连接共享内存标识符为shmid的共享内存，连接成功后把共享内存区对象映射到调用进程的地址空间，随后可像本地空间一样访问



int shmdt(const void *shmaddr); //shmaddr是shmat()函数的返回值

从进程的地址空间分离共享内存



int shmctl(int shmid, int cmd, struct shmid_ds *buf)

控制共享内存，包括得到、复制、删除



**大胆的想法：**

​	单独launch

 world_model +  control   omnivision节点

