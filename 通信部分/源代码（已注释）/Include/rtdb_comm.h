/*
 * Frederico Miguel Santos - frederico.miguel.santos@gmail.com
 * CAMBADA robotic soccer team - www.ieeta.pt/atri/cambada
 * University of Aveiro
 * Copyright (C) 2009
 *
 * This file is part of RTDB middleware.
 * http://code.google.com/p/rtdb/
 *
 * RTDB middleware is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RTDB middleware is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RTDB middleware.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __RTDB_COMM_H
#define __RTDB_COMM_H

#include "rtdb_api.h"


//	*************************
//	DB_comm_put: Escreve na base de dados - apenas para outros agentes!
//  向其他机器人写入数据库
//	Entrada:
//		int _agent = numero do agente  机器人编号
//		int _id = identificador da 'variavel'  id
//		void *_value = ponteiro com os dados   带数据的指针
//		int life = tempo de vida da 'variavel' em ms  变量生命周期（毫秒）
//	Saida:
//		0 = OK
//		-1 = erro
//
int DB_comm_put (int _agent, int _id, int _size, void *_value, int life);


//	*************************
//	DB_comm_ini: 
//  （猜测）分配其他机器人的访问权限
//	Entrada:
//		int _id = array com identificadores da 'variavel'   具有变量标识符的数组
//		int _size = array com tamanhos da 'variavel'  可变大小的数组
//		int _period = array com periodos da 'variavel'  可变间隔数组
//	Saida:
//		int n_shared_recs = numero de 'variaveis' shared  共享的变量数
//		-1 = erro
//
int DB_comm_ini(RTDBconf_var *rec);


#endif
