# ZED环境配置

## 1.zed所需环境

```
1.zed sdk.
2.cuda11.1.
3.nvidia显卡驱动（最好适配cuda11.1）
```

## 2.ZED SDK

```
 1.在zed官网https://www.stereolabs.com/zed/ 下载对应系统的sdk。注意看其适配的cuda版本。
 2 $chmod 777 ./ZED_SDK_Linux_x86_64_v0.9.2c_beta.run //修改文件为可执行文件（sdk版本文件可修改）
   $./ZED_SDK_Linux_x86_64_v0.9.2c_beta.run  //执行文件
```

## 3.CUDA安装

```
参考官网https://developer.nvidia.com/cuda-11.1.0-download-archive 选择对应的版本和系统
```

## 4.注意事项

```
1.zed sdk有对nvidia显卡的硬性要求。
2.一般来说cuda安装包中自带显卡驱动，所以不需要重新安装显卡驱动。
3.若是双系统，一般来说针对自己的笔记本电脑，安装前一定要将BIOS中的SecureBoot设置为disabled。
4.在安装cuda时，最好进入非图像界面，并关闭Ubuntu自带的显卡驱动。

```

参考网址： https://zhuanlan.zhihu.com/p/258914264?utm_source=qq&utm_medium=social&utm_oi=1254013768511434752