# zed_ros安装

## 1.install the zed-ros-wrapper:

```
$ cd ~/catkin_ws/src
$ git clone https://github.com/stereolabs/zed-ros-wrapper.git
$ cd ../
$ rosdep install --from-paths src --ignore-src -r -y
$ catkin_make -DCMAKE_BUILD_TYPE=Release
$ source ./devel/setup.bash
```

注：在ros netice中rosdep可不用,下同。

## 2.install zed-ros-example

```
$ cd ~/catkin_ws/src
$ git clone https://github.com/stereolabs/zed-ros-examples.git
$ cd ../
$ rosdep install --from-paths src --ignore-src -r -y
$ catkin_make -DCMAKE_BUILD_TYPE=Release
$ source ./devel/setup.bash
```

## 3.starting the zed node

```
ZED camera: $ roslaunch zed_wrapper zed.launch

```

注：zed目前有三个版本-zed，zedMini，zed2。不同的版本打开节点不同具体可参见官网，目前基地使用的是zed即初代版本。

## 4.using rviz

```
$ roslaunch zed_display_rviz display_zed.launch
```

## 5.Dynamic reconfigure（动态参数改）

```
You can dynamically change many configuration parameters during the execution of the ZED node.

You can set the parameters using the command dynparam set, e.g.:

$ rosrun dynamic_reconfigure dynparam set depth_confidence 80
or you can use the GUI provided by the rqt stack:

$ rosrun rqt_reconfigure rqt_reconfigure
```



参考网址：https://www.stereolabs.com/zed/