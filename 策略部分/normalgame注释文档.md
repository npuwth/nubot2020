# Normalgame

​	This file is for the detail “selections” confronted with different kinds of situations and is tailored for different roles. 

> ​	备注：  由于nuc上的当前代码和目前阅读的代码有些许差异（可能有部分被注释或修改），待上机再后期更新。
>
> ​                                           By Aidan Lew

[TOC]

> 

![1605320962752](assets/1605320962752.png)

**Tip：**  

​	dribbleState   是针对全场所有机器人的吗，还是针对某一个机器人？哪个更好

​     在normalGame里是否也需要设置一个变量记录我方所有机器人是否有一个抓着球。



**参考文件：**

  core.hpp：场地信息，具体的线坐标

  plan：具体的找球功能实现（behavior更底层控制代码）



**补充Actions**

​	大多数都没有实际意义，只是一个常量作为标记（从任意一个角色的策略代码里可以看出这点）

​	但是，selection = action 可以理解为“下一步的计划是：XXX action”

```c++

	Stucked =0,
	Penalty =1,
 	CanNotSeeBall =2,
	SeeNotDribbleBall =3,
	TurnForShoot =4,
    AtShootSituation =5,
	TurnToPass=6,   
	StaticPass =7,  
	AvoidObs=8,
	Catch_Positioned = 9,   
	Positioned =10,          
	Positioned_Static  =11, 
	No_Action  =12, 
//no action的话 就会把所有速度制为零 无论你赋了多少值 
```

  	因此不能通过英文单词的字面意思来揣测某一步策略的意图，要通过后续的调用的my_plan函数以及其中的参数。













### NormalGame::process(int role_)

```c++

//根据传入的机器人信息role_选取合适的策略
//目前代码里只有四个： 
    asActive(); //攻击
	asAssist(); //辅助
	asGaolie(); //防守
	one2one();  //1v1对抗
//如果不是防守，就让它动起来？  Line 49
```



### asActive()  进攻机器人

```c++
1，是否看到球：（以下均隐藏"seleted_action_="，即计划下一步的action）
	-如果没看到，No_Action
	-如果抓着球，Positioned
	-如果球在敌方那边，Catch_Positioned
	-否则，Positioned_Static	
	
2,如果1到了最后一步：（即1中球在自己这半场且没抓着球）
	-移动到目标位置，由于在在自己半场，先沿着y方向移动，再转向（可优化）
	move2Positionwithobs_noball(DPoint(0.0, ball_pos_.y_/2.0));
	-旋转朝向目标位置
	positionAvoidObs(ball_pos_);

3，如果1到了Catch_Positioned	（即1中球在敌方半场）
	m_plan_->CatchBall(); 去抓球
	
4，如果1到了Positioned(即1中是抓着球的)
	-即计划是移动到射门位置
	-旋转朝向目标位置positionAvoidObs
	
	如果：朝向角度在攻击范围角里面，则
		 -移动到目标位置move2Positionwithobs_noball
		 -下一步计划：AtShootSituation
	如果：距离在攻击范围之内，则：
		 -下一步计划：StaticPass
		 
5，如果4到了StaticPass，即已经到达射门位置，准备射门（有疑问）
	-移动到一个合适射门的地方
	move2Positionwithobs_noball
	(DPoint(-FIELD_LENGTH*1.0/2+50.0, 0.0)) //x参数设置？
    -旋转朝向球门
    positionAvoidObs2（球门方向）
    -检测当前朝向与目标的弧度差，如果误差低于一个阈值且velzero()==true
    则标记AtShootSituation,可以射门了。
```



### asAssisit()   辅助机器人  

> 备注： 此块代码应该有问题，不完整且或许不是“辅助”角色。

```c++
1，是否看到球：（以下均隐藏"seleted_action_="，即计划下一步的action）
	-如果没看到，No_Action
	-如果抓着球，StaticPass
	-如果球在自己半场，Catch_Positioned
	-否则，Positioned_Static	
	
2,如果1到了最后一步：（即1中球在对方半场）
	-移动到参数中指向的位置（有疑问）
	 m_plan_->move2Positionwithobs_noball(DPoint(-100.0, ball_pos_.y_/2.0))  //？？为何要往左走
	-旋转朝向目标位置
	positionAvoidObs(ball_pos_);

3，如果1到了Catch_Positioned	（即1中球在己方半场）
	-如果球的x坐标位置在一个阈值左边，且y的坐标位置在一个区间内
	m_plan->catchBall(); 去抓球
	
4，如果1到了StaticPass(即1中是抓着球的)
	-旋转朝向目标位置positionAvoidObs，此时目标是敌方球门中心
	-检测朝向和目标弧度差，如果小于一个阈值且velzero==true
    （此处的阈值与进攻的机器人有点不同）
    	 AtShootSituation;
```





### asGoalie（）防守机器人

```c++
1，如果抓着球：
 	-AtShootSituation；

2，否则:
 	-检测是否看到球：
 		-如果isLocationKnown==false.CanNotSeeBall
		-否则. Positioned
3,如果2到了CanNotSeeBall(即1中是没有看到球的)
        -目标位置设置为 target
        target=DPoint(-FIELD_LENGTH*1.0/2+RADIUS-10.0, 0.0)
		-移动到目标位置：（为了视野开阔，但是作为守门员不能出去太远）
		positionAvoidObs(DPoint(0.0,0.0))  //朝向球场中心
		move2PositionWithons_noball(target)//移动

4，如果2到了Positioned
	-说明可能对方抓到了球，且要射门了（有疑问）
	-target设置：利用时间预测y方向的走势
  DPoint(-FIELD_LENGTH*1.0/2+RADIUS-10.0,     	ball_pos_.y_+ball_vel_.y_*time_robot2ball)
        
    -根据target的y绝对值制定策略：
      -如果绝对值大于防守区域：
      即：fabs(target.y_) >           			fabs(ourGoalPosl.y_)-RADIUS  
      【其中RADIUS指的是机器人的大小】
      【部分参数】
    -ourGoalPosl=(-300,80);
	-RADIUS=20（define.h)	 		                       30(strategydefine.hpp)
     
     -绝对值超过防守区域，故要开始防守：
       move2Positionwithobs_noball(target);
       positionAvoidObs(ball_pos_);
	  设置y方向上的速度，如果在【10，90】区间则设为90

```





### one2one（）  1V1 对抗赛

```c++
1，是否看到球：（以下均隐藏"seleted_action_="，即计划下一步的action）
	-如果没看到，No_Action
	-如果抓着球，Positioned
	-否则，Catch_Positioned	
	
2,如果1到了Catch_Positioned	（即1中没抓到球）
	m_plan_->CatchBall(); 去抓球

3，如果1到了Positioned	（即1中已抓到球）
	-即计划是移动到射门位置
	-旋转朝向目标位置positionAvoidObs
	
	如果：朝向角度在攻击范围角里面，则
		 -移动到目标位置move2Positionwithobs_noball
		 -下一步计划：AtShootSituation
	如果：距离在攻击范围之内，则：
		 -下一步计划：StaticPass
		 
4，如果3到了StaticPass，即已经到达射门位置，准备射门（有疑问）
	-移动到一个合适射门的地方
	move2Positionwithobs_noball
	(DPoint(-FIELD_LENGTH*1.0/2+50.0, 0.0)) //x参数设置？
    -旋转朝向球门
    positionAvoidObs2（球门方向）
    -检测当前朝向与目标的弧度差，如果误差低于一个阈值且velzero()==true
    则标记AtShootSituation,可以射门了。

```





### update（）： 更新场地信息

```c++
更新的信息包括：
（从world_model->XXXInfo[world_model_->AgentID-1]获取数据）

直接获取的信息：
   机器人位置
   机器人朝向
   球的位置
   球的速度
   敌人的机器人尺寸
   场地障碍物尺寸
   
   opp_robot_[]来存敌方的三个机器人
   离球最近的对手
   离自己最近的对手
   离敌方球门最近的对手

由推断获取的信息：
   ● 如果机器人和球都在场地中心：无动作
   ● 如果敌方机器人尺寸算出来为0：敌方在场地中心
   ● 如果正在持球：
   		setLocatioKnown（true）
   		且球的位置在机器人的位置
   		
传信息：
	是否可踢球
	是否抓着球
```



### velzero（）

​	最终决定是否AtShootSituation的一个函数







### init():  初始化属性

```
机器人位置
机器人朝向
球的位置
球的速度
初始动作——无动作
是否可射击：不可
```



