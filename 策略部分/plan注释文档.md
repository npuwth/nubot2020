# Plan.ccp 

(头文件 plan.cpp/ core.cpp。 使用了nubot的命名空间)

# 主要函数：

### 1. paln()

​       作用：判断机器人在我方半场还是敌方半场，给相应的变量（inourfield,inoppfield)赋值。

​       

###   2.catch ball部分：

####         catchBall()

​              作用：根据球的速度判断机器人是追静态的球（调用catchMotionlessBall函数）还是动态的球（调用catchMovingBall函数）。球的速度小于10就认为是静态的。

####         catchBallForCoop()（ 球进入机器人下面的抓球结构）

​              实现功能：调用catchBallSlowly函数。 当球即将被抓住时，缓慢抓球可以防止球被弹开或者球不能被一次抓住。

####         catchBallSlowly() （缓慢向球移动，等球接近）

​       实现功能：首先positionAvoidObs函数使机器人朝向球的位置。再判断机器人需要转动的角度（小于5/180*派），调用move2Positionwithobs_noball函数。

####        catchMovingBall(double maxvel, double maxw)  （抓住移动的球）

​           数据：机器人追到球的时间，球未来的位置（因为球在移动）。

​          实现功能：首先判断机器人是否追到球（没有），计算球和机器人的位置差值（DPonit），调用positionAvoidObs2函数，使机器人朝向球。然后判断球与机器人的距离，采用先靠近再逼近的策略。调用move2Positionwithobs_noball函数，使机器人靠近球。

####        catchMotionlessBall(DPoint target, double maxvel, double maxw)（抓住静态的球）

​    实现功能：计算目标位置与机器人位置的差值，调用positionAvoidObs2函数，使机器人朝向球，然后先靠近再逼近。调用move2Positionwithobs_noball函数，使机器人追到球。





​                                      /************postion部分************/

####        positionAvoidObs(DPoint target, double maxw, double angle_thres) （朝向目标位置1）

​             实现功能：计算目标位置与机器人位置的差值，调用positionAvoidObs2函数实现转向。



####        positionAvoidObs2(double targetangle, double maxw, double angle_thres) （朝向目标位置）

​         实现功能：当目标角度和机器人的朝向相差大于限定值时，调用behaviour中的rotate2AbsOrienation函数实现转向。否则，调用setAppw函数 ，使机器人角速度置零。



####          move2Positionwithobs_noball(DPoint target, double distance_thres, float maxvel, float maxacc, bool avoid_ball) （移动到目标位置，比如：追球）

​     实现功能：当机器人与目标点的距离大于限制值（这里主要考虑机器人和球的直径）调用subtarget函数生成子目标点（避障与路径规划），再调用move2Position函数沿着子目标点到达目标点。直到机器人与目标点的距离小于限制值时，调用setAppvx函数和setAppvy函数将机器人速度置零。

 

####            pnpoly(const std::vector<DPoint> & pts, const DPoint & test_pt)

​              判断一个坐标是否在一个多边形区域内



####                 update()

​          更新球和机器人的位置信息









​                                 ///////////// round with soccer 部分////////////



####            ballRoundTrack()

​                 球的路径追踪，包括对球和机器人位置的输出，机器人的速度。



####             oppneartargetid(DPoint target) 

​                            目标位置附近敌方机器人的数量

####               ourneartargetid(DPoint target, int avoidid) 

​                             目标位置附近我方机器人的数量