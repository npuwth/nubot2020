# 策略代码介绍与注解

Behaviour是一个类用于控制机器人追球。//最底层实现，实现机器人到达某一些指定点的方法。

```c
basicPDControl//使用位置式PD控制减小误差，
    
basicPDControl2//使用增量式PD控制减小误差，更加稳定

move2Position//使用PID控制机器人到达指定位置，如果距离近(距离小于10)就视为已经抵达，如果距离远，那么预期的速度与距离是一个线性函数，因子是0.7。但是速度是有上限的，如果线性函数的结果大于最大值，那么多出的部分就减去。我们由此得到的我们期望的加速度，最后再使用上述的PD控制使得小车达到预期点

move2target/*加入了追踪算法，预计球的下一步位置，预计时间为机器人最快抵达球的时间*2(这个参数可以改)之后的球的位置，这样的话，距离球越远，预计球更久之后的位置*/

rotate2AbsOrienation//转向 和控制速度的函数一样，同样使用了pid，容许的角度偏差是5度
    
rotatetowardsRelPoint//调用上面那个函数进行转向
//这两个函数是单独的，控制距离的自己控制距离，转向的自己控制转向   

accelerateLimit//限制最大的加速度，前后的速度变化差不会很大，车的行进更加平稳。
```

```c++
对于 Class World_Model_Info 的注解
```

对于 World_Model_Info 的详细定义在  world_model_info .h 中62行

到代码的最后。以下简要介绍它的功能。

场上敌我双方机器人的信息，球的信息，障碍物的信息，有关球的信息。

```c++
对于 DribbleState 的注解
```

使用 is_dribble_ s来表示当前是否持球

使用set resets is_dribble_ 的值

------



```
对于class plan
```

函数解释

```c++
positionAvoidObs//末尾的Obs应该是指不要出界,转向
/*
positionAvoidObs和positionAvoidObs2联合使用，用于转向
*/
    
move2Positionwithobs_noball//这个函数规定如果离球过近速度就调成0
    
```

------

对于subtargets (避障算法)

```
Min_num//去寻找给定数组中值最小的index
Min//给出给定数组中最小的值
Max_num 与Max同理
```