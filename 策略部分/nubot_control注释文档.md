# nubot_control.cpp

从视觉模块，里程计，世界模型获取足球，里程计，障碍物，敌人，机器人，教练等信息，根据模式做出决策，给出电机速度，策略，持球，射门等指令。

## 1.通信部分

***接收***：（msg）（从视觉，里程计，世界模型接收）

**ballinfo3d**:  足球3D信息；类型： Point3d pos ；Point3d velocity；bool pos_known_3d ;  bool pos_known_2d ; bool velocity_known 

**odoinfo**:  里程计信息； 类型： float32 Vx; float32 Vy；float32 w；float32 angle；bool BallIsHolding; bool RobotStuck; bool PowerState

**gyroinfo**:  也是里程计信息（具体作用不清楚）

**obstaclesinfo**:  障碍物信息；类型：Point2d[]  pos; PPoint[]  polar_pos

**worldmodelinfo**：  世界模型信息；类型：ObstaclesInfo  obstaclesinfo; ObstaclesInfo oppinfo; RobotInfo[]  robotinfo; BallInfo[]  ballinfo;  CoachInfo  coachinfo; PassCommands  pass_cmd

即障碍物信息，敌人信息，机器人信息（主要包含位置，朝向，持球等等），足球信息（位置，速度），教练信息（比赛模式等），传球信息（收发点，动态还是静态传球等）

***发布***：（msg）（发布给hwcontroller硬件控制）

**motor_cmd_pub**_: 电机速度；类型：bool   stop_; int32  Vx; int32  Vy; int32  w

**strategy_info_pub:**一些策略信息；类型：int32  AgentID; int32  targetNum1; int32  targetNum2;  int32  targetNum3;  int32 targetNum4;  int32  staticpassNum;  int32  staticcatchNum;  uint32  role;  uint32  action;  bool  is_dribble;  bool  is_kickoff;  float32  role_time;  PassCommands  pass_cmd

***客户：***（srv）（hwcontroller提供服务）

ballhandle_client_:持球服务；发送类型：int32  enable（使能信号）;  接收类型：int32  BallIsHolding （正在持球）

shoot_client_:射门服务；发送类型：int32  strength（射门强度）；int32  ShootPos（射门位置）；接收类型：int32  ShootIsDone（射门完成）



###### *注*：这里的接收和发布用到的是ros中的topic同步通信，客户用到的是service异步通信。

###### *思考*1：为什么ballhandle与shoot要用service通信而不用topic通信？

## 2.时间控制

maxknowtime: 阈值，10ms

ROLEMAXTIME:500

known_time:  知道球位置的时间

wait_time: 用于kickoff模式中

## 3.主循环 loopcontrol

模式选择：

#####        测试模式

1. STOPROBOT：停止，调用clearBehaviorState

2. PARKINGROBOT：初始站位，调用move2Positionwithobs_noball和positionAvoidObs2

3. OUR_KICKOFF: 开球，若持球一定时间则踢球

4. CHECKGIGE：测试摄像头是否正常，抓静止的球，调用catchMotionlessBall

   ##### 正式比赛

5. STARTROBOT：正式比赛，调用normalgame中的update和process，传入role_, 如果kickenable，则射门

   ##### 技术挑战赛

6. LOCATION：调用location中的update和process

7. AVOIDANCE：调用avoidance中的update和process

8. PASSING：调用passing中的update和process，如果kickenable，则射门

## 4.更新信息

***update_my_dribble_info:***  更新我的持球信息

从里程计获取自己是否正在持球，并在world_model_info_中更新

若我正在持球，则认为球的位置已知，球的位置就是机器人的位置

***update_world_model_info:***  更新各种世界信息

机器人信息，障碍物信息，足球信息，m_plan信息

###### *思考2：以上两个函数更新信息的激发是什么？*

## 5.获取参数

***getrosparam***：（从nubot_control.launch文件中获取）

1. 获取比赛模式

2. 获取各比赛模式下所需的参数

   **LOCATION：**五个定位点

   **PASSING：**level

   **CHECKGIGE：**check_gige_

   **STARTROBOT:**  role_

***configure:***

获取pid参数信息kp___，kalpha__，kbeta_

## 6.输出

“输出的东西即为运行的过程” 

"start  control  process"

“initialize  control  process”

若agent为空，“this  agent  number  is  not  read  by  robot”

“MATCHMODE=。。。”

若模式为startrobot，“role_=。。。”

“V：。。。，。。。，。。。”

“pos：。。。，。。。”

“shoot”

## 7.总结

一个类：NubotControl类

函数大全：

构造/析构函数

getrosparam：获取比赛模式，角色等参数

configure：获取pid参数

update_my_dribble_info:更新自己的持球信息（消息处理)

update_world_model_info:更新各种世界信息(消息处理)

balllinfo3dcallback：球的3维信息（消息处理）

loopcontrol：总控制循环

setEthercatCommand：对速度进行最终处理发送给硬件控制

setshoot：射门，持球信息的客户，发送给硬件控制

isballhandle：看编号为id的机器人是否持球，update_my_dribble_info中调用

调用关系：

**1.**main调用构造函数   **->**  **2.** 构造函数进行通信声明，机器人标号读取等初始化   **->**  **3.**  获取参数（configure和getrosparam）**->**  **4.** 通过时间控制，每隔10ms调用一次loopcontrol  **->**  **5.** 根据参数进入模式，执行相应的动作   **->**   **6.**调用setshoot，setEthercatCommand 发送控制信息给hwcontroller硬件控制结点

###### *注：5中执行相应的动作其实就是计算出3个速度（x，y，w），给出两个请求（ shoot 和 ballhandle）*

------



```
思考1答案：ballhandle与shoot使用service通信是因为这两个信息的调用频率并不高（topic是订阅机制，不断的把信息发布在topic上，接收者不断地订阅。service只有在客户发出请求时服务者才会响应，并能给出一个反馈表示是否执行成功）。

思考2答案：以上两个函数的调用并不是在总控制loopcontrol中，而是消息机制激发的，因为同步通信，所以只要消息发生改变，world_model中的信息也会即时更新
```